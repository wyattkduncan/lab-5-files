#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    FILE *s;
    s = fopen("homelistings.csv", "r");
    if (!s)
    {
        printf("Can't open homelistings.csv file\n");
        exit(1);
    }
    FILE *large;
    large = fopen("large.txt", "w");
    if (!large)
    {
        printf("Can't open big.txt\n");
        exit(1);
    }
    
    FILE *small;
    small = fopen("small.txt", "w");
    if (!small)
    {
        printf("Can't open small.txt\n");
        exit(1);
    }
    FILE *medium;
    medium = fopen("medium.txt", "w");
    if (!medium)
    {
        printf("Can't open medium.txt\n");
        exit(1);
    }
    
    char address[30];
    int zip, id, price, bed, bath;
    int cheapest = 100000000;
    int area =0;
    double average;
    while (fscanf(s, "%d,%d,%[^,],%d,%d,%d,%d ", &zip, &id, address, &price, &bed, &bath, &area) != EOF)
    {
        if (area < 1000)
        {
            fprintf(small,"%s : %d \n", address, area);
        }
        else if (area > 2000)
        {
            fprintf(large, "%s : %d \n", address, area);
        }
        else
        {
            fprintf(medium, "%s : %d \n", address, area);
        }
        
    }
    fclose(s);
    fclose(large);
    fclose(small);
    fclose(medium);
    
}