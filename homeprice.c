#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

int main()
{
    FILE *s;
    s = fopen("homes200.csv", "r");
    if (s == NULL)
    {
        perror("Can't open homes200.csv file\n");
        exit(1);
    }
    // zip , ID, adress, price, bed, baths, area.
    // 95816,80017179,328 22nd St,525000,4,2,1448
    char address[30];
    int zip, id, area, price, bed, bath;
    int cheapest = 100000000;
    int expensive =0;
    double sum =0;
    double counter=0;
    double average;
    while (fscanf(s, "%d,%d,%[^,],%d,%d,%d,%d ", &zip, &id, address, &price, &bed, &bath, &area) != EOF)
    {
    if (price > expensive)
        {
            expensive = price;
            //strcpy(expensive_house, address);
        }
    if (price < cheapest)
        {
            cheapest = price;
        }
    sum = price + sum;
    counter++;
    }
    average = sum / counter;
    fclose(s);
    printf("%d, %d, %f \n", cheapest, expensive, average);
    
}